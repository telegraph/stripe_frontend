apiApp.directive('navigation', function ($state, Admin) {

    return {
        templateUrl: '/navigation/navigation.directive.ejs',
        scope: {},
        link: function (scope, element) {
        	scope.userLogout = function () {
        		Admin.userLogout().then(function (successfulSend) {
                    $state.go('user-login');
                });
        	}

        }
    };

});
