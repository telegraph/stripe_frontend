apiApp.factory('Accounts', function ($http) {

	var accounts = {};

	accounts.createAccount = function (data) {
		return $http.post('/stripe/account/create', data).then(function response(response) {
			return response.data;
		})
		.catch(function (err) {
			throw new Error(err.message);
		});

	}

	accounts.getAccounts = function () {
		return $http.get('/stripe/accounts/get').then(function response(response) {
			return response.data;
		})
		.catch(function (err) {
			throw new Error(err.message);
		});
	}

	accounts.getSingleAccount = function (data) {
		return $http.get('/stripe/accounts/get/single/' + data).then(function response(response) {
			return response.data;
		})
		.catch(function (err) {
			throw new Error(err.message);
		});
	}

	accounts.editSingleAccount = function (data) {
		return $http.put('/stripe/account/update/:' + accountID, data).then(function response (response) {
			return response.data;
			})
		.catch(function (err) {
            throw new Error(err.message);
    	});
	}

	return accounts;
});