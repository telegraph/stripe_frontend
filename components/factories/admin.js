apiApp.factory('Admin', function ($http) {

	var admin = {};

	admin.getUser = function (credentials) {
		return $http.post('/users/login/', credentials)
		.then(function success (response) {
			console.log(response.data);
			return response.data;
		})
		.catch(function (err) {
			throw new Error(err.message);
		});

	}

	admin.userLogout = function () {
		return $http.get('/users/logout').then(function response(response) {
			return response.data;
		})
		.catch(function (err) {
			throw new Error(err.message);
		});
	}

	admin.createNewUser = function (credentials) {
		console.log('hitting the admin create user function');
		return $http.post('/users/register/', credentials)
		.then(function success (response) {
			console.log(response.data);
			return response.data;
		})
		.catch(function (err) {
			throw new Error(err.message);
		});
	}

	return admin;
});