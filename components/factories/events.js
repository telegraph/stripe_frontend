apiApp.factory('Events', function ($http) {

	var events = {};

	events.getEvents = function () {
		return $http.get('/stripe/events/get').then(function response(response) {
			return response.data;
		})
		.catch(function (err) {
			throw new Error(err.message);
		});
	}

	return events;
});