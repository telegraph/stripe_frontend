apiApp.config(function ($stateProvider) {
	$stateProvider.state('dashboard', {
		url: '/',
		controller: 'DashboardController',
		templateUrl: '/dashboard/dashboard.ejs'
	})
	.state('dashboard.viewAccounts', {
		url: 'accounts/',
		templateUrl: '/dashboard/dashboard.viewAccounts.ejs'
	})
	.state('dashboard.newAccount', {
		url: 'new-account/',
		controller: 'NewAccountController',
		templateUrl: '/dashboard/dashboard.newAccount.ejs'
	})
	.state('dashboard.viewEvents', {
		url: 'events/',
		templateUrl: '/dashboard/dashboard.viewEvents.ejs'
	})
	.state('dashboard.updateAccount', {
		url: 'edit-account/:accountID/',
		templateUrl: '/dashboard/dashboard.updateAccount.ejs'
	});
});

apiApp.controller('DashboardController', function () {

});

apiApp.controller('EventsController', function ($scope, Events) {
	$scope.getEvents = function () {
		Events.getEvents().then(function (events) {
			$scope.events = events;
		});
	}

	Events.getEvents().then(function (events) {
		$scope.events = events;
	});
});

apiApp.controller('AccountsController', function ($scope, Accounts) {
	$scope.getAccounts = function () {
		Accounts.getAccounts().then(function (accounts) {
			$scope.accounts = accounts;
		});
	}

	Accounts.getAccounts().then(function (accounts) {
		$scope.accounts = accounts;
	});

});

apiApp.controller('NewAccountController', function ($scope, $state, Accounts) {

	// $scope.example = {
	// 	dob: ''
	// }

	$scope.createAccount = function () {
		$scope.submitted = true;

		console.log($scope.newAccount);

		$scope.newAccount.name = $scope.newAccount.first_name + ' ' + $scope.newAccount.last_name;
		Accounts.createAccount($scope.newAccount).then(function(successfulSend) {
			// $state.go('message-success');
			console.log('hey');
		});
	};
});

apiApp.controller('EditAccountController', function ($scope, $state, $stateParams, Accounts) {
	$scope.submitted = true;
	console.log($scope.account);

	$scope.account.name = $scope.account.first_name + ' ' + $scope.account.last_name;

	$scope.editAccount = function ($stateParams) {
		Accounts.editSingleAccount($stateParams.accountID, $scope.account).then(function(successfulSend) {
			console.log('edited correctly');
		});
	}
	

	Accounts.getSingleAccount($stateParams.accountID).then(function(data) {
		$scope.account = data;
	});

});

