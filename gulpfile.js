// All used modules.
var gulp = require('gulp');
var babel = require('gulp-babel');
var runSeq = require('run-sequence');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var notify = require('gulp-notify');

var sass = require('gulp-sass');
var rename = require('gulp-rename');

var livereload = require('gulp-livereload');


var sourcemaps = require('gulp-sourcemaps');


gulp.task('reload', function () {
    livereload.reload();
});

gulp.task('reloadCSS', function () {
    return gulp.src('./public/style.css').pipe(livereload());
});

gulp.task('buildJS', function () {
    return gulp.src(['./angular-app.js', './components/**/*.js'])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(babel())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./public'));
});

gulp.task('buildCSS', function () {

    var sassCompilation = sass();
    sassCompilation.on('error', console.error.bind(console));

    return gulp.src('./components/main.scss')
        .pipe(plumber({
            errorHandler: notify.onError('SASS processing failed! Check your gulp process.')
        }))
        .pipe(sassCompilation)
        .pipe(rename('style.css'))
        .pipe(gulp.dest('./public'));
});

gulp.task('build', function () {
    runSeq(['buildJS', 'buildCSS']);
});

gulp.task('default', function () {

    gulp.start('build');

    gulp.watch('./components/**/*.js', function () {
        runSeq('buildJS', 'reload');
    });

    gulp.watch(['./components/**/*.ejs', './views/index.ejs'], ['reload']);


    gulp.watch('./components/**/*.scss', function () {
        runSeq('buildCSS', 'reloadCSS');
    });

    livereload.listen();

});
