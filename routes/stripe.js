var express = require('express');
var stripe = require("stripe")(
  "sk_test_qTXDNGkBIqpfpDNLn24ndqqF"
);

var router = express.Router();

//////////////  ACCOUNT CREATION  ////////////////
router.post('/stripe/account/create', function(req, res, next) {
  console.log("message")
  console.log(req.body)
  res.status.send(204);
  // var account = req.body


  //   var legal_entity = {
  //     "address": {
  //       "city": account.city,
  //       "country": "US",
  //       "line1": account.address1,
  //       "line2": account.address2,
  //       "postal_code": account.zip,
  //       "state": account.state
  //     },
  //     "dob": {
  //       "day": account.dob_day,
  //       "month": account.dob_month,
  //       "year": account.dob_year
  //     },
  //     "first_name": account.first_name,
  //     "last_name": account.last_name,
  //     "type": "company",
  //     "business_name": account.name,
  //     "business_tax_id": account.tax_id,
  //     "personal_id_number": account.personal_id_number,
  //     "ssn_last_4": account.last_4
  // }

  // var external_account = {
  //   "object": "bank_account",
  //   "account_holder_name": account.account_holder_name,
  //   "account_holder_type": account.account_holder_type,
  //   "bank_name": account.bank_name,
  //   "country": "US",
  //   "currency": "usd",
  //   "default_for_currency": false,
  //   "last4": "6789",
  //   "metadata": {
  //   },
  //   "routing_number": account.routing_number,
  //   "account_number": account.account_number,
  //   "status": "new"
  // }

  //   stripe.accounts.create({
  //     managed: true,
  //     country: 'US',
  //     business_name: account.name,
  //     business_url: account.url,
  //     email: account.email,
  //     external_account: external_account,
  //     legal_entity: legal_entity,
  //     tos_acceptance: {
  //       date: Math.floor(Date.now() / 1000),
  //       ip: req.connection.remoteAddress
  //     }
  //   }, function(err, stripe_account) {
  //     console.log(err)
  //     console.log(stripe_account)
  //     res.send(stripe_account)
  //   });
});

//////////////  VIEW ALL ACCOUNTS  ////////////////
router.get('/stripe/accounts/get', function(req, res, next) {
  stripe.accounts.list(
    function(err, accounts) {
      res.send(accounts)
    })
})

//////////////  VIEW SINGLE ACCOUNT  ////////////////
router.get('/stripe/accounts/get/single/:id', function(req, res, next) {
  var account_id = req.params.id
  stripe.accounts.retrieve(
    account_id,
    function(err, account) {
      res.send(account)
    }
  );
})

//////////////  UPDATE SINGLE ACCOUNT  ////////////////
router.post('/stripe/account/create', function(req, res, next) {
  console.log("message")
  console.log(req.body)
  var account = req.body

  var legal_entity = {
    "address": {
      "city": account.city,
      "country": "US",
      "line1": account.address1,
      "line2": account.address2,
      "postal_code": account.zip,
      "state": account.state
    },
    "dob": {
      "day": account.dob_day,
      "month": account.dob_month,
      "year": account.dob_year
    },
    "first_name": account.first_name,
    "last_name": account.last_name,
    "type": "company",
    "business_name": account.name,
    "business_tax_id": account.tax_id,
    "personal_id_number": account.personal_id_number,
    "ssn_last_4": account.last_4
}

var external_account = {
  "object": "bank_account",
  "account_holder_name": account.account_holder_name,
  "account_holder_type": account.account_holder_type,
  "bank_name": account.bank_name,
  "country": "US",
  "currency": "usd",
  "default_for_currency": false,
  "last4": "6789",
  "metadata": {
  },
  "routing_number": account.routing_number,
  "account_number": account.account_number,
  "status": "new"
}
  stripe.accounts.update("acct_18FWayGm472CZQFW", {
    managed: true,
    country: 'US',
    business_name: account.name,
    business_url: account.url,
    email: account.email,
    external_account: external_account,
    legal_entity: legal_entity
  })
  res.send("success")
})


//////////////  RECIEVE EVENTS  ////////////////
router.post('/stripe/events/save', function(req, res, next) {
  var body = req.body
  console.log(body)
  var db = req.db
  var collection = db.get('events')
  collection.insert(body,function(err, doc) {
    res.send()
  })
})

//////////////  COLLECT EVENTS  ////////////////
router.get('/stripe/events/get', function(req, res, next) {
  var db = req.db
  var collection = db.get('events')
  collection.find({},{},function(err, docs) {
    res.send(docs)
  })
})

module.exports = router;
