var apiApp = angular.module('apiApp', ['ui.router']);

//attach ui-router
apiApp.config(function ($locationProvider, $urlRouterProvider) {
	// This turns off hashbang urls (/#about) and changes it to something normal (/about)
    $locationProvider.html5Mode(true);

	// For any unmatched url, redirect to '/'
	$urlRouterProvider.otherwise("/");
	
});